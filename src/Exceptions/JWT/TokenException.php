<?php

namespace Agrolu\Commons\Exceptions\JWT;

use Exception;

class TokenException extends Exception
{
    public function render()
    {
        return response()->json([
            'message' => "Token inválido"
        ], 401);
    }
}
