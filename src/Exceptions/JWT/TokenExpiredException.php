<?php

namespace Agrolu\Commons\Exceptions\JWT;

use Exception;

class TokenExpiredException extends Exception
{
    public function render()
    {
        return response()->json([
            'message' => "Token expirado"
        ], 401);
    }
}
