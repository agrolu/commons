<?php

namespace Agrolu\Commons\Exceptions\JWT;

use Exception;

class TokenNotFoundException extends Exception
{
    public function render()
    {
        return response()->json([
            'message' => "Authorization é obrigatório"
        ], 401);
    }
}
