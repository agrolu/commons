<?php

namespace Agrolu\Commons\Constants;

class UserData
{
    private static string $userId;

    public static function getUser()
    {
        return self::$userId;
    }

    public static function setUser(string $userId)
    {
        self::$userId = $userId;
    }
}