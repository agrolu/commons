<?php

namespace Agrolu\Commons\Traits;

use Illuminate\Support\Str;

trait Uuid
{
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Str::uuid()->toString();
        });
    }

    /**
      * Get the value indicating whether the IDs are incrementing.
      *
      * @return bool
      */
     public function getIncrementing()
     {
         return false;
     }
     
    /**
      * Get the auto-incrementing key type.
      *
      * @return string
      */
     public function getKeyType()
     {
         return 'string';
     }
}