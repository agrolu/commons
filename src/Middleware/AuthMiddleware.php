<?php

namespace Agrolu\Commons\Middleware;

use Agrolu\Commons\Constants\UserData;
use Closure;
use Agrolu\Commons\Exceptions\JWT\TokenExpiredException;
use Agrolu\Commons\Exceptions\JWT\TokenException;
use Agrolu\Commons\Exceptions\JWT\TokenNotFoundException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\Key;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;

class AuthMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            if (! $request->bearerToken()) {
                throw new TokenNotFoundException();
            }

            $decoded = JWT::decode(
                $request->bearerToken(),
                new Key(
                    "-----BEGIN PUBLIC KEY-----\n" . env('JWT_PUBLIC_KEY') . "\n-----END PUBLIC KEY-----",
                    'RS256'
                )
            );
            UserData::setUser($decoded->claims->id);
            
            return $next($request);
        } catch (ExpiredException $expiredException) {
            throw new TokenExpiredException();
        } catch (TokenNotFoundException $tokenNotFoundExcepton) {
            throw $tokenNotFoundExcepton;
        } catch (\Throwable $throwable) {
            throw new TokenException();
        }
    }
}