<?php

namespace Agrolu\Commons\Helpers;

use Illuminate\Http\UploadedFile;

class ImageHelper
{
    public static function getBytes(UploadedFile $image)
    {
        return fread(fopen($image->getPathName(), 'r'), $image->getSize());
    }
}