<?php

namespace Agrolu\Commons\Services\AWS;

use Aws\Sqs\SqsClient;

class SqsService
{
    private SqsClient $sqsClient;

    public function __construct()
    {
        $this->sqsClient = new SqsClient([
            'version' => 'latest',
            'region' => env('AWS_REGION', 'us-east-1')
        ]);
    }

    public function sendMessage(array $payload)
    {
        try {
            return $this->sqsClient->sendMessage([
                'MessageBody' => json_encode($payload),
                'QueueUrl' => env('AWS_QUEUE_URL'),
            ]);
        } catch (\Throwable $exception) {
            throw new AwsException($exception->getMessage());
        }
    }
}