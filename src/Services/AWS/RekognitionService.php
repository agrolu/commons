<?php

namespace Agrolu\Commons\Services\AWS;

use Agrolu\Commons\Helpers\ImageHelper;
use Aws\Rekognition\RekognitionClient;
use Illuminate\Http\UploadedFile;

class RekognitionService
{
    private RekognitionClient $rekognitionClient;

    public function __construct()
    {
        $this->rekognitionClient = new RekognitionClient([
            'version' => 'latest',
            'region' => env('AWS_REGION', 'us-east-1')
        ]);
    }

    public function compareFaces(UploadedFile $sourceFile, UploadedFile $targetFile)
    {
        try {
            return $this->rekognitionClient->compareFaces([
                'SourceImage' => [
                    'Bytes' => ImageHelper::getBytes($sourceFile)
                ],
                'TargetImage' => [
                    'Bytes' => ImageHelper::getBytes($targetFile)
                ]
            ]);
        } catch (\Throwable $exception) {
            throw new AwsException($exception->getMessage());
        }
    }

    public function detectModerationLabels(UploadedFile $file)
    {
        try {
            return $this->rekognitionClient->detectModerationLabels([
                'Image' => [
                    'Bytes' => ImageHelper::getBytes($file)
                ]
            ]);
        } catch (\Throwable $exception) {
            throw new AwsException($exception->getMessage());
        }
    }

    public function detectText(UploadedFile $file)
    {
        try {
            return $this->rekognitionClient->detectText([
                'Image' => [
                    'Bytes' => ImageHelper::getBytes($file)
                ]
            ]);
        } catch (\Throwable $exception) {
            throw new AwsException($exception->getMessage());
        }
    }

    public function detectLabels(UploadedFile $file)
    {
        try {
            return $this->rekognitionClient->detectLabels([
                'Image' => [
                    'Bytes' => ImageHelper::getBytes($file)
                ]
            ]);
        } catch (\Throwable $exception) {
            throw new AwsException($exception->getMessage());
        }
    }
}
