<?php

namespace Agrolu\Commons\Services\AWS;

use Exception;

class AwsException extends Exception
{
    public function render()
    {
        return response()->json([
            'message' => $this->getMessage()
        ], 500);
    }
}
