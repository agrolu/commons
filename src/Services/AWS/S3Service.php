<?php

namespace Agrolu\Commons\Services\AWS;

use Agrolu\Commons\Helpers\ImageHelper;
use Aws\S3\S3Client;
use Illuminate\Http\UploadedFile;
use Ramsey\Uuid\Uuid;

class S3Service
{
    private S3Client $s3Client;

    public function __construct()
    {
        $this->s3Client = new S3Client([
            'version' => 'latest',
            'region' => env('AWS_REGION', 'us-east-1')
        ]);
    }

    public function putObject(
        UploadedFile $file,
        array $key,
        string $contentType = 'image/png'
    ) {
        try {
            return $this->s3Client->putObject([
                'Bucket' => config('aws.s3.bucket'),
                'Key' => $key['path'] . Uuid::uuid4(),
                'Body' => ImageHelper::getBytes($file),
                'ACL' => $key['acl'],
                'ContentType' => $contentType
            ]);
        } catch (\Throwable $exception) {
            throw new AwsException($exception->getMessage());
        }
    }
}
