<?php

namespace Agrolu\Commons\Services\AWS;

use Aws\Sns\SnsClient;

class SnsService
{
    private SnsClient $snsClient;

    public function __construct()
    {
        $this->snsClient = new SnsClient([
            'version' => 'latest',
            'region' => env('AWS_REGION', 'us-east-1')
        ]);
    }

    public function publish($message, int $number)
    {
        try {
            return $this->snsClient->publish([
                'Message' => $message,
                'PhoneNumber' => $number
            ]);
        } catch (\Throwable $exception) {
            throw new AwsException($exception->getMessage());
        }
    }
}
