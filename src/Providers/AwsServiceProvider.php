<?php

namespace Agrolu\Commons\Providers;

use Agrolu\Commons\Services\AWS\RekognitionService;
use Agrolu\Commons\Services\AWS\S3Service;
use Agrolu\Commons\Services\AWS\SnsService;
use Agrolu\Commons\Services\AWS\SqsService;
use Illuminate\Support\ServiceProvider;

class AwsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'rekognition',
            function ($app) {
                return new RekognitionService();
            }
        );
        
        $this->app->singleton(
            's3',
            function ($app) {
                return new S3Service();
            }
        );
        
        $this->app->singleton(
            'simpleNotificationService',
            function ($app) {
                return new SnsService();
            }
        );
        
        $this->app->singleton(
            'simpleQueueService',
            function ($app) {
                return new SqsService();
            }
        );
    }
}